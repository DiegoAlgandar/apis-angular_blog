﻿using Blog_API.Models;
using Blog_API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaController : ControllerBase
    {
        private Categorias_Service _categorias_Service;
        public CategoriaController(Categorias_Service categorias_Service) => _categorias_Service = categorias_Service;
       
        [HttpGet("obtenerCategorias")]
       // [ValidateAntiForgeryToken]
        public IEnumerable<Categorias_Model> obtenerEntradas()
        {
            return _categorias_Service.obtenerCategorias();
        }

        [HttpPost("insertarCategoria")]
       // [ValidateAntiForgeryToken]
        public string insertarCategoria(Categorias_Model model)
        {
            try
            {
                var r = _categorias_Service.guardarCategoria(model);
                return r;
            }
            catch (Exception ex)
            {
                return "-1";
            }
        }
    }
}
