﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog_API.Models;
using Blog_API.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Blog_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private Blog_Service _blog_Service;

        public PostController(Blog_Service blog_Service)
        {
            _blog_Service = blog_Service;
        }
        // GET: api/<testController>
        [HttpGet("obtenerEntradas")]
        //[ValidateAntiForgeryToken]
        public IEnumerable<PostConsulta> obtenerEntradas()
        {
            return _blog_Service.obtenerEntradas();
        }

        // GET api/<testController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<testController>
        [HttpPost]
      //  [ValidateAntiForgeryToken]
        public ActionResult Post([FromBody] Posts newPost)
        {
            try
            {
                var result =_blog_Service.SaveNewPost(newPost);
                if (result == "-1") return StatusCode(500);
                return Ok(result);
                
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<testController>/5
        [HttpPut("{id}")]
        //[ValidateAntiForgeryToken]
        public string Put(int id)
        {
            var result = _blog_Service.updatePost(id);
            return result;

        }

        [HttpPut("cambiarEstado/{id}")]
       // [ValidateAntiForgeryToken]
        public string CambiarEstado(int id)
        {
            var result = _blog_Service.updatePost(id);
            return result;

        }

        // DELETE api/<testController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
