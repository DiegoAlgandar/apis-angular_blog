﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog_API.Models;
using Blog_API.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Blog_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private Users_Service _user_Service;

        public UsersController(Users_Service user_Service)
        {
            _user_Service = user_Service;
        }
        // GET: api/<UsersController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<UsersController>/5
        [HttpGet("{user}/{password}")]
       // [ValidateAntiForgeryToken]
        public Users Get(string user,string password)
        {
            var userfound= _user_Service.getUserLogin(user, password);
            return userfound;
        }

        // POST api/<UsersController>

        [HttpPost("newUser")]
       // [ValidateAntiForgeryToken]
        public ActionResult newUser([FromBody] Users newUser)
      

        {
            try
            {
                var result = _user_Service.SaveNewUser(newUser);
                if (result == "-1") return StatusCode(500);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<UsersController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<UsersController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
