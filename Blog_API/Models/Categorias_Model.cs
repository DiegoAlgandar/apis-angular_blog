﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog_API.Models
{
    public class Categorias_Model
    {
        public int ID { get; set; }

        public string Nombre { get; set; }
    }
}
