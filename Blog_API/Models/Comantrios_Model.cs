﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog_API.Models
{
    public class Comantrios_Model
    {
        public int id { get; set; }
        public int id_publicacion { get; set; }
        public int  id_usuario{ get; set; }
        public string comentario { get; set; }
        public string fecha { get; set; }
    }

    public class GetComentarios_Model 
    {
        public string nombre_usuario { get; set; }
        public string comentario { get; set; }
        public string fecha { get; set; }

    }
}
