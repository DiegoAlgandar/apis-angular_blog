﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Blog_API.Models
{
    public class Posts
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int usuario_id { get; set; }
        [Required]
        public int categoria_id { get; set; }
        [Required(ErrorMessage = "Your post need a title")]
        public string titulo { get; set; }
        [Required(ErrorMessage = "There's no content in your post")]
        public string descripcion { get; set; }

        public string fecha { get; set; }

    }

    public class PostConsulta {
        public int id { get; set; }
        public string titulo { get; set; }
        public string descripcion { get; set; }
        public string fecha { get; set; }
        public string nombreUser { get; set; }
        public string categoria { get; set; }
        public Boolean showComments { get; set; }
        public IEnumerable<GetComentarios_Model> comentarios { get; set; }


    }
}
