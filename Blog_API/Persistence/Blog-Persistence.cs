﻿using Blog_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog_API.Persistence
{
    public class Blog_Persistence
    {
        private Connections _connections;

        public Blog_Persistence(Connections connections) => _connections = connections;

        public string savepostBlog(Posts Post) {
            string pResult = "";
            string pSetencia = 
                "INSERT INTO entradas " +
                "(usuario_id,categoria_id,titulo,descripcion,fecha) values (@USUARIO_ID,@CATEGORIA_ID,@TITULO,@DESCRIPCION,@FECHA)";

            try
            {
                _connections.executeBlog(pSetencia, (com) =>
                {
                    com.Parameters.AddWithValue("@USUARIO_ID",Post.usuario_id);
                    com.Parameters.AddWithValue("@CATEGORIA_ID", Post.categoria_id);
                    com.Parameters.AddWithValue("@TITULO", Post.titulo);
                    com.Parameters.AddWithValue("@DESCRIPCION", Post.descripcion);
                    com.Parameters.AddWithValue("@FECHA", DateTime.Now.ToShortDateString());

                    pResult = com.ExecuteNonQuery().ToString();
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                pResult = "-1";
            }

            return pResult;
        }

        public IEnumerable<PostConsulta> obtenerEntradas() {

            string pSentence = @"SELECT entradas.id,titulo, show, descripcion,categorias.nombre AS 'Categoria',entradas.fecha, usuarios.nombre AS 'Usuario' FROM ENTRADAS entradas inner join CATEGORIAS categorias on entradas.categoria_id=categorias.id inner join 
USUARIOS usuarios on entradas.usuario_id=usuarios.id";


            try
            {
              return  _connections.queryBlogListNoAsync(pSentence, new Dictionary<string, object> { { "", "" } }, reader => {

                  return new PostConsulta()
                  {
                      id = reader["id"] is DBNull ? 0 : Convert.ToInt32(reader["id"].ToString()),
                      titulo = reader["titulo"] is DBNull ? null : reader["titulo"].ToString(),
                      descripcion = reader["descripcion"] is DBNull ? null : reader["descripcion"].ToString(),
                      //nombre = reader["nombre"] is DBNull ? null : reader["nombre"].ToString(),
                      fecha = reader["fecha"] is DBNull ? null : reader["fecha"].ToString(),
                      nombreUser = reader["Usuario"] is DBNull ? null : reader["Usuario"].ToString(),
                      categoria = reader["Categoria"] is DBNull ? null : reader["Categoria"].ToString(),
                      showComments = reader["show"] is DBNull ? false : Convert.ToBoolean(reader["show"]),
                      //comentarios= ObtenerComentarios()

                  };
                });
            }
            catch (Exception ex)
            {

                throw;
            }
        }
 
    public string updatePost(int ID)
    {
        string pResult = "";
            string pSetencia =
                "IF( ((SELECT show FROM entradas WHERE id = @ID)=0) ) " +
                "UPDATE entradas Set show = 1 where id = @ID " +
                "ELSE Update entradas Set show = 0 where id = @ID ";

        try
        {
            _connections.executeBlog(pSetencia, (com) =>
            {
                
                com.Parameters.AddWithValue("@ID", ID);


                pResult = com.ExecuteNonQuery().ToString();
            });
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
            pResult = "-1";
        }

        return pResult;
    }
        public string estadoInicial(int ID)
        {
            string pResult = "";
            string pSetencia = "UPDATE entradas Set show = 0 where id = @ID ";
               

            try
            {
                _connections.executeBlog(pSetencia, (com) =>
                {

                    com.Parameters.AddWithValue("@ID", ID);


                    pResult = com.ExecuteNonQuery().ToString();
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                pResult = "-1";
            }

            return pResult;
        }


        public IEnumerable<GetComentarios_Model> ObtenerComentarios(int id)
        {

            string pSentence = @"select usua.nombre,com.mensaje,com.fecha from entradas en inner join Comentarios com on 
                            en.id=com.id_publicacion inner join usuarios usua on usua.id=com.id_usuario where en.id=@ID";

            try
            {
                return _connections.queryBlogListNoAsync(pSentence, new Dictionary<string, object> { { "@ID", id } }, reader => {

                    return new GetComentarios_Model()
                    {
                        nombre_usuario = reader["nombre"] is DBNull ? null : reader["nombre"].ToString(),
                        comentario = reader["mensaje"] is DBNull ? null : reader["mensaje"].ToString(),
                        fecha = reader["fecha"] is DBNull ? null : reader["fecha"].ToString(),
                    };
                });
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public string addComment(Comantrios_Model model)
        {
            string pResult = "";
            string pSetencia =
                "INSERT INTO Comentarios " +
                "(id_usuario,id_publicacion,mensaje,fecha) values (@USUARIO_ID,@PUBLICACION_ID,@MENSAJE,@FECHA)";

            try
            {
                _connections.executeBlog(pSetencia, (com) =>
                {
                    com.Parameters.AddWithValue("@USUARIO_ID", model.id_usuario);
                    com.Parameters.AddWithValue("@PUBLICACION_ID", model.id_publicacion);
                    com.Parameters.AddWithValue("@MENSAJE", model.comentario);
                    com.Parameters.AddWithValue("@FECHA", DateTime.Now.ToShortDateString());

                    pResult = com.ExecuteNonQuery().ToString();
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                pResult = "-1";
            }

            return pResult;
        }
    }
}
