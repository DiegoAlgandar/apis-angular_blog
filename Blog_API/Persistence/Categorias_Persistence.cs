﻿using Blog_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog_API.Persistence
{
    public class Categorias_Persistence
    {
        private Connections _connections;
        public Categorias_Persistence(Connections connections) => _connections = connections;

        public string guardarCategoria(Categorias_Model model) {
            string pResult = "";
            string pSentence = "insert into categorias (nombre) values (@NOMBRE)";

            try
            {
                _connections.executeBlog(pSentence, (com) => {
                    com.Parameters.AddWithValue("@NOMBRE",model.Nombre);
                    pResult = com.ExecuteNonQuery().ToString();
                });
            }
            catch (Exception ex)
            {
                pResult = "-1";
            }

            return pResult;
        }
        public IEnumerable<Categorias_Model> obtenerCategorias()
        {

            string pSentence = "SELECT * FROM categorias";

            try
            {
                return _connections.queryBlogListNoAsync(pSentence, new Dictionary<string, object> { { "", "" } }, reader => {

                    return new Categorias_Model()
                    {
                        ID = reader["id"] is DBNull ? 0 : Convert.ToInt32(reader["id"].ToString()),
                        Nombre = reader["nombre"] is DBNull ? null : reader["nombre"].ToString(),
                        
                    };
                });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
