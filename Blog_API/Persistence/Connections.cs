﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Blog_API.Persistence
{
    public class Connections
    {
        private SqlConnection GetConnectionKasse()
        {
            return new SqlConnection(new SqlConnectionStringBuilder()
            {
                UserID = "sa",
                Password = "uts",
                DataSource = ".",
                InitialCatalog = "blog",
                ConnectTimeout = 15
            }.ConnectionString);

        }

 
        //Insercion a la base de datos
        public void executeBlog(string ASentencie, Action<SqlCommand> ASentencieAction)
        {
            using (SqlConnection con = GetConnectionKasse())
            {

                try
                {
                    using (SqlCommand com = new SqlCommand(ASentencie, con))
                    {

                        con.Open();
                        ASentencieAction.Invoke(com);
                    }
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                    {
                        con.Close();
                        con.Dispose();
                    }
                }


            }
        }

        public IEnumerable<T> queryBlogListNoAsync<T>(string ASentencie, Dictionary<string, object> AParams, Func<SqlDataReader, T> f)
        {
            List<T> pResult = new List<T>();

            using (SqlConnection con = GetConnectionKasse())
            {

                try
                {
                    using (SqlCommand com = new SqlCommand(ASentencie, con))
                    {
                        foreach (Match match in Regex.Matches(ASentencie, @"(?<!\w)\@\w+"))
                        {
                            string pKey = match.Value.ToUpper();
                            com.Parameters.AddWithValue(pKey, AParams[pKey]);

                        }
                        con.Open();
                        using (SqlDataReader reader = com.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                pResult.Add(f.Invoke(reader));
                            }
                        }

                    }
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                    {
                        con.Close();
                        con.Dispose();
                    }
                }
            }
            return pResult;
        }




        public T executeReaderBlogNoAsync<T>(string ASentencia, Dictionary<string, object> AParams, Func<SqlDataReader, T> AData)
        {
            try
            {
                T pResult = default(T);

                SqlConnection con = GetConnectionKasse();
                SqlCommand com = new SqlCommand(ASentencia, con);

                foreach (Match match in Regex.Matches(ASentencia, @"(?<!\w)\@\w+"))
                {
                    string pKey = match.Value.ToUpper();
                    com.Parameters.AddWithValue(pKey, AParams[pKey]);
                }

                con.Open();

                SqlDataReader reader = com.ExecuteReader();

                if (reader.Read())
                    pResult = AData(reader);

                if (con.State == System.Data.ConnectionState.Open)
                    con.Close();

                return pResult;
            }
            catch
            {
                return default(T);
            }
        }
    }
}
