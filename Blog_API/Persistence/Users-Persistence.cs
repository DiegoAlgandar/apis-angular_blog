﻿using Blog_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog_API.Persistence
{
    public class Users_Persistence
    {
        private Connections _connections;

        public Users_Persistence(Connections connections) => _connections = connections;

        public string saveUser(Users user)
        {
            string pResult = "";
            string pSetencia =
                "INSERT INTO usuarios " +
                "(nombre,apellidos,email,password,fecha) values (@NOMBRE,@APELLIDOS,@EMAIL,@PASSWORD,@FECHA)";

            try
            {
                _connections.executeBlog(pSetencia, (com) =>
                {
                    com.Parameters.AddWithValue("@NOMBRE", user.nombre);
                    com.Parameters.AddWithValue("@APELLIDOS", user.apellidos);
                    com.Parameters.AddWithValue("@EMAIL", user.email);
                    com.Parameters.AddWithValue("@PASSWORD", user.password);
                    com.Parameters.AddWithValue("@FECHA", DateTime.Now.ToShortDateString());

                    pResult = com.ExecuteNonQuery().ToString();
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                pResult = "-1";
            }

            return pResult;
        }

        public Users getUserLogin(string email,string password)
        {
            string pSentence = @"select id,nombre,apellidos,email,password  from usuarios where email=@EMAIL and password =@PASSWORD";

            return _connections.executeReaderBlogNoAsync(pSentence, new Dictionary<string, object> { { "@EMAIL",email }, { "@PASSWORD", password } }, (reader) =>
            {
                return new Users()
                {
                    id= reader["id"] is DBNull ? 0 : int.Parse(reader["id"].ToString()),
                    nombre = reader["nombre"] is DBNull ? null : reader["nombre"].ToString(),
                    apellidos = reader["apellidos"] is DBNull ? null : reader["apellidos"].ToString(),
                    email = reader["email"] is DBNull ? null : reader["email"].ToString(),
                    password= reader["password"] is DBNull ? null : reader["password"].ToString(),
                };
            });


        }

    }
}
