﻿using Blog_API.Models;
using Blog_API.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog_API.Services
{
    public class Blog_Service
    {
        private Blog_Persistence _blog_Persistence;

        public Blog_Service(Blog_Persistence blog_Persistence) => _blog_Persistence = blog_Persistence;

        public string SaveNewPost(Posts postmodel) => _blog_Persistence.savepostBlog(postmodel);

        //public IEnumerable<PostConsulta> obtenerEntradas() => _blog_Persistence.obtenerEntradas();

        public IEnumerable<PostConsulta> obtenerEntradas()
        {
           var res= _blog_Persistence.obtenerEntradas();
            foreach (var item in res)
            {
                item.comentarios=_blog_Persistence.ObtenerComentarios(item.id);
            }
            return res;
        }
        public string updatePost(int id) => _blog_Persistence.updatePost(id);
        public string cambiarEstado(int id) => _blog_Persistence.estadoInicial(id);
        public string AddComment(Comantrios_Model model) => _blog_Persistence.addComment(model);



    }
}
