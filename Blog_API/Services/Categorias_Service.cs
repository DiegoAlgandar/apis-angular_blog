﻿using Blog_API.Models;
using Blog_API.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog_API.Services
{
    public class Categorias_Service
    {
        private Categorias_Persistence _categorias_Persistence;

        public Categorias_Service(Categorias_Persistence categorias_Persistence) => _categorias_Persistence = categorias_Persistence;

        public string guardarCategoria(Categorias_Model model) => _categorias_Persistence.guardarCategoria(model);

        public IEnumerable<Categorias_Model> obtenerCategorias() => _categorias_Persistence.obtenerCategorias();
    }
}
