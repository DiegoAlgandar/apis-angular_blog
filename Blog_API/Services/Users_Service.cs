﻿using Blog_API.Models;
using Blog_API.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog_API.Services
{
    public class Users_Service
    {
        private Users_Persistence _blog_Persistence;

        public Users_Service(Users_Persistence users_Persistence) => _blog_Persistence = users_Persistence;
        public string SaveNewUser(Users newUSer) => _blog_Persistence.saveUser(newUSer);
        public Users getUserLogin(string email,string password) => _blog_Persistence.getUserLogin(email,password);
    }
}
